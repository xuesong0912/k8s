package com.example.k8s;

import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static com.alibaba.nacos.api.config.ConfigType.YAML;

@SpringBootApplication
@NacosPropertySource(dataId = "configTest",groupId = "group1",type = YAML,autoRefreshed = true)
public class K8sApplication {

    public static void main(String[] args) {
        SpringApplication.run(K8sApplication.class, args);
    }

}
