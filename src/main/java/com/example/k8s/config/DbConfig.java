package com.example.k8s.config;

//import com.alibaba.druid.pool.DruidDataSource;
//import org.mybatis.spring.SqlSessionFactoryBean;
//import org.mybatis.spring.SqlSessionTemplate;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.core.io.Resource;
//import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
//import org.springframework.core.io.support.ResourcePatternResolver;
//
//import javax.sql.DataSource;
//import java.io.IOException;

//@Configuration
//@PropertySource("classpath:druid-config.properties")
//public class DbConfig {
//
//    @Value("${druid.driverClassName}")
//    private String driverClassName;
//    @Value("${druid.url}")
//    private String url;
//    @Value("${druid.user}")
//    private String username;
//    @Value("${druid.password}")
//    private String password;
//    @Value("${druid.maxActive}")
//    private int maxActive;
//
//    @Bean
//    public DataSource dataSource(){
//        DruidDataSource ds = new DruidDataSource();
//        ds.setDriverClassName(driverClassName);
//        ds.setUrl(url);
//        ds.setUsername(username);
//        ds.setPassword(password);
//        ds.setMaxActive(maxActive);
//        ds.setMinIdle(0);
//        return ds;
//    }
//
//    @Bean
//    public SqlSessionFactoryBean sqlSessionFactory() throws IOException {
//        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
//        //加载匹配到的xml映射配置
////        Resource[] resources =  resolver.getResources("classpath*:com/nd/mathlearning/server/*/dao/mapper/*.xml");
//        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
//        bean.setDataSource(dataSource());
//        //加载Mybatis配置
//        bean.setConfigLocation(resolver.getResource("classpath:mybatis/mybatis-config.xml"));
//        return bean;
//    }
//
//    @Bean
//    public SqlSessionTemplate sqlSessionTemplate() throws Exception {
//        SqlSessionTemplate bean = new SqlSessionTemplate(sqlSessionFactory().getObject());
//        return bean;
//    }
//
//}
