package com.example.k8s.controller;

import com.alibaba.nacos.api.annotation.NacosInjected;
import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import com.example.k8s.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/test")
@RestController
public class TestController {

    @Autowired
    TestService testService;

    @NacosValue(value = "${test.config:defaultConfig}",autoRefreshed = true)
    private String testConfig;

    @NacosInjected
    private NamingService namingService;

    @GetMapping("/")
    public String test(){
        return "hello docker v1";
    }

    @GetMapping("/sql")
    public String testSql(){
        return testConfig;
    }

    @GetMapping(value = "/get")
    @ResponseBody
    public List<Instance> get(@RequestParam String serviceName) throws NacosException {
        return namingService.getAllInstances(serviceName);
    }


    @GetMapping("/nacos")
    public String testNacos(){
        return "hello nacos v1";
    }
}
